#!/bin/bash

echo "************************************************************"
echo "*   At various points, you will be prompted to choose a    *"
echo "*   version of a module to install. Read the options and   *"
echo "*   select whichever version is 'Recommended'.             *"
echo "************************************************************"
echo
read -p "Press return to continue ... " -n1 -s
echo

# we definitely want this script to bail and stop upon error
set -e

PROJECT_SOURCE=https://downloads.ch.cam.ac.uk/drupal-projects/release-history

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

if [ ! -f "$SCRIPTDIR/external_users.sql" ] ; then
  echo $SCRIPTDIR/external_users.sql not found. See README
  exit 1
fi

if [ ! -f "$SCRIPTDIR/external_cache_symplectic.sql" ] ; then
  echo $SCRIPTDIR/external_cache_symplectic.sql not found. See README
  exit 1
fi

SKIP_EXTERNAL_SQL=false

while getopts "hsi:e:d:t:" OPTION; do
  case $OPTION in
    h)
      echo "Usage: makesite.sh -i internal_url [-e external_url] -d site_title [-t site_type]"
      echo 
      echo "  internal_url : backend URL (without protocol), e.g. www-smith.ch.private.cam.ac.uk"
      echo "  external_url : proxied URL (without protocol), e.g. www.ch.cam.ac.uk/group/smith"
      echo "  site_title : title displayed on webpages, e.g. The Smith Group"
      echo "  site_type : only 'research' is currently supported. Read the script to see what it does"
      exit 1
      ;;
    s)
      SKIP_EXTERNAL_SQL=true
      ;;
    e)
      PUBLIC_URL=$OPTARG
      ;;
    i)
      PRIVATE_URL=$OPTARG
      ;;
    d)
      SITETITLE=$OPTARG
      ;;
    t)
      SITETYPE=$OPTARG
      ;;
    *)
      echo "Unknown option"
      exit 1
      ;;
  esac
done

if [ -z "$SITETITLE" ] ; then
  read -e -p "Enter human-friendly site title: " SITETITLE
fi

if [ -z "$PRIVATE_URL" ] ; then
  read -e -p "private FQDN of site (e.g. www-smith.ch.private.cam.ac.uk): " PRIVATE_URL
fi

if [ -n "$PUBLIC_URL" ] ; then
  BASEURL=$PUBLIC_URL
else
  BASEURL=$PRIVATE_URL
fi

SITENAME=$(echo $PRIVATE_URL | cut -d '.' -f 1 | sed s/^www-//)
SERVERSHORTNAME=$(echo $PRIVATE_URL | cut -d '.' -f 1)

DBNAME=$(echo $SITENAME | tr '-' '_')
DBUSER=$DBNAME

SITEDIR=/var/www/drupal/$SITENAME
SETTINGSFILE=$SITEDIR/sites/default/settings.php

DRUSH="/usr/local/bin/drush -y -r $SITEDIR"

APACHECONFFILE=/etc/apache2/sites-available/${SERVERSHORTNAME}.conf

if [ -e "$SITEDIR" ] ; then
  echo $SITEDIR already exists. Bailing out.
  exit 1
fi

if [ -e "$APACHECONFFILE" ] ; then
  echo $APACHECONFFILE already exists. Bailing out.
  exit 1
fi

if [ -f /root/.my.cnf ] ; then
  MYSQL="mysql"
else
  MYSQL="mysql --defaults-file=/etc/mysql/debian.cnf"
fi

# set up database
SQLCMDFILE=$(mktemp)

DBPASS=$(LC_ALL=C tr -dc A-Za-z0-9 2>/dev/null </dev/urandom | dd bs=12 count=1 2>/dev/null)

cat<<EOF >$SQLCMDFILE
create database $DBNAME;
grant all on $DBNAME.* to '$DBUSER'@'localhost' identified by '$DBPASS';
EOF

$MYSQL < $SQLCMDFILE
rm $SQLCMDFILE

sed "s#__SERVERURL__#$PRIVATE_URL#g" $SCRIPTDIR/templates/apache2  | sed "s#__SERVERSHORTNAME__#$SERVERSHORTNAME#g" | sed "s#__SITEDIR__#$SITEDIR#g" > $APACHECONFFILE

mkdir -p /var/log/apache2/${SERVERSHORTNAME}

mkdir $SITEDIR
pushd $SITEDIR

$DRUSH make $SCRIPTDIR/chemistry.yml

# elfinder library comes with these files, which we don't want because drush will
# complain endlessly about security problems otherwise
rm sites/all/libraries/elfinder/connectors/php/connector.php
rm sites/all/libraries/elfinder/elfinder.php.html

mkdir files
chown -R www-data:www-data files
chmod g+s files

mkdir -p /var/log/apache2/${SERVERSHORTNAME}
$DRUSH site-install standard --account-name=admin --account-pass=$DRUPAL_PASS --db-url=mysql://$DBUSER:$DBPASS@localhost/$SITENAME

# needed for later when we run drush block-disable.
if [ ! -d $HOME/.drush/drush_extras ] ; then $DRUSH dl drush_extras; fi

$DRUSH vset date_first_day 0
$DRUSH vset date_default_timezone 'Europe/London'

if ! ${SKIP_EXTERNAL_SQL} ; then
  $($DRUSH sql-connect) < ${SCRIPTDIR}/external_users.sql
  echo "include_once('/etc/drupal-chem/external_users.php');" >>$SETTINGSFILE
fi

cat<<EOF  >>$SETTINGSFILE
include_once('/etc/drupal-chem/chemistry_db.php');
EOF

# This module provides all sorts of settings we like, including e.g.
# creating our default roles. Some of our later modules assign default
# permissions and so we want to be sure the roles are available.
$DRUSH en chemistry_defaults
# make sure that everything provided by chemistry_defaults really
# has taken effect
$DRUSH cc all

# imagecrop has no "recommended" version so need to explicitly specify which version we want
$DRUSH dl imagecrop-1.x-dev


# we don't want the "article" content type
$DRUSH php-eval "node_type_delete('article')"

$DRUSH vset site_name "$SITETITLE"
$DRUSH vset site_mail "support@ch.cam.ac.uk"

$DRUSH en statistics
$DRUSH vset statistics_count_content_views 1 
$DRUSH vset statistics_count_content_views_ajax 1 
$DRUSH vset statistics_enable_access_log 1
$DRUSH vset statistics_flush_accesslog_timer "2419200"

# disable inbuilt cron: we'll just use a real cron job!
$DRUSH vset cron_safe_threshold 0

# Added by default from 7.100, but we don't want it
# https://www.drupal.org/node/3424912
$DRUSH pm-disable announcements_feed
$DRUSH pm-uninstall announcements_feed

# who to notify about updates? for now: no-one
$DRUSH php-eval "variable_set('update_notify_emails', array())"

$DRUSH vset file_public_path files

# Allow visitors to register accounts, otherwise raven won't work properly
$DRUSH vset user_register 1

$DRUSH dis comment

# Theme
$DRUSH en cambridge_theme, chemistry_theme
$DRUSH vset theme_default chemistry_theme
$DRUSH dis bartik
$DRUSH php-eval "variable_set('theme_chemistry_theme_settings', array('search_box' => 2, 'search_box_filter_inst' => 'CHEM'))"

$DRUSH block-disable --module=search --delta=form
$DRUSH block-disable --module=system --delta=navigation
$DRUSH block-disable --module=system --delta=help
$DRUSH block-disable --module=system --delta=powered-by
$DRUSH block-disable --module=user --delta=login

# Raven, and associated variables
$DRUSH en raven
# block access to the usual Drupal login/registration stuff
$DRUSH vset raven_login_override 1
# but permit user/backdoor/login for non-raven admin login
$DRUSH vset raven_backdoor_login 1
# and let raven-authenticated users automatically have accounts created on login
$DRUSH vset raven_override_administrator_approval 1

# Drupal modules
$DRUSH en views,views_ui,ckeditor,elfinder,content_menu,mpac
$DRUSH en ldap_authorization_drupal_role
$DRUSH en menu_block,pathauto,token,diff,redirect
$DRUSH en pagepreview,advanced_help,context_ui,find_content

# MISD modules
$DRUSH en cambridge_image_styles,cambridge_teasers, cambridge_related_links, cambridge_link,cambridge_carousel

# the following is meant to be set by chemistry_defaults. However, it doesn't stick...
# perhaps related to https://drupal.org/node/1224970
# also, do this before enabling chemistry_defaults as that creates an index page
$DRUSH vset pathauto_node_pattern "[node:title]"

# Chemistry modules

$DRUSH en chemistry_always_visible,chemistry_menu_blocks,chemistry_slideshow,chemistry_ldap,chemistry_utilities,chemistry_microsite_breadcrumb,chemistry_faqs
$DRUSH en chemistry_ga
$DRUSH en nodequeue
$DRUSH en chemistry_nodequeue_views
$DRUSH en chemistry_footer
$DRUSH en content_access_audit

$DRUSH vset chemistry_microsite_breadcrumb_link 'https://www.ch.cam.ac.uk'
$DRUSH vset chemistry_microsite_breadcrumb_title 'Yusuf Hamied Department of Chemistry'

$DRUSH en chemistry_block_access

$DRUSH ch-create-node page Index
$DRUSH vset site_frontpage "node/1"

$DRUSH dl field_validation-7.x-2.6
$DRUSH en field_validation field_validation_ui


if [ "${SITETYPE}" == "research" ] ; then
  $DRUSH dl --source=$PROJECT_SOURCE chemistry_staff_page,chemistry_research_page,chemistry_symplectic,chemistry_feeds_sql
  # even though chemistry_staff_page depends on chemistry_feeds_sql, we need to explicitly enable it first. Otherwise,
  # we end up with a broken staff importer - maybe the enablement order is undefined, or maybe there's a cache somewhere,
  # but regardless, chemistry_staff_page ends up with a broken fetcher configuration.
  $DRUSH en chemistry_feeds_sql
  $DRUSH en chemistry_staff_page
  $DRUSH en feeds_ui
  $DRUSH en chemistry_research_page
  $DRUSH php-eval "user_role_grant_permissions(4, array('add terms in staff_category'))"
  $DRUSH php-eval "user_role_grant_permissions(2, array(
     'edit own staff_page content',
     )
   )"
  $DRUSH en chemistry_symplectic
  if ! ${SKIP_EXTERNAL_SQL} ; then
    $($DRUSH sql-connect) < ${SCRIPTDIR}/external_cache_symplectic.sql
  fi
  echo "include_once('/etc/drupal-chem/elements.php');" >> $SETTINGSFILE
fi

if [ -n "$PUBLIC_URL" ]; then
TMPFILE=`mktemp`
PUBLICPATH=""
if [[ $PUBLIC_URL =~ ^www.ch.cam.ac.uk/+ ]] ; then
  PUBLICPATH=`echo $PUBLIC_URL | cut -d '/' -f 2-`
fi

cat <<EOF > $TMPFILE
<?php

\$urls = array('http://$PUBLIC_URL', 'http://$PRIVATEURL', '/$PUBLICPATH');
\$url = implode("\r\n", \$urls);
variable_set('pathologic_local_paths', \$url);
EOF

$DRUSH php-script $TMPFILE
rm $TMPFILE


if [[ -n $PUBLICPATH ]] ; then 
cat <<EOF >>$SETTINGSFILE
\$cookie_domain='.ch.cam.ac.uk';

if(isset(\$_SERVER['HTTP_X_PROXIED_PROTO']) && \$_SERVER['HTTP_X_PROXIED_PROTO'] == 'https') {
  \$proto = 'https';
} else {
  \$proto = 'http';
}

\$base_url = "\$proto://$PUBLIC_URL";
\$_SERVER['REQUEST_URI'] = '/$PUBLICPATH'.\$_SERVER['REQUEST_URI'];

EOF
else 
cat <<EOF >>$SETTINGSFILE
\$cookie_domain='.cam.ac.uk';

if(!isset(\$_SERVER['HTTP_X_FORWARDED_HOST'])) {
  \$base_url = 'http://$PRIVATEURL';
} else {
  \$base_url = 'http://$PUBLIC_URL';
}
EOF
fi

fi

# enable revisions for the 'page' content type
$DRUSH php-eval "variable_set('node_options_page', array(0 => 'status', 1 => 'revision'))"

# Strange. The first enable doesn't lead to the block settings being set correctly.
# But, if we en/dis/en everything works. Need to dig deeper into how the different
# parts of the feature work
$DRUSH en chemistry_site_editor_block
$DRUSH dis chemistry_site_editor_block
$DRUSH en chemistry_site_editor_block

# some menus we don't want
$DRUSH php-eval "menu_delete(array('menu_name' => 'user-menu'))"
$DRUSH php-eval "menu_delete(array('menu_name' => 'features'))"

# although there's a module to export permissions into a feature, it's buggy..
# N.B. chemistry_defaults will create a "site admin" role, which is probably guaranteed to be rid=4 
# at this stage. We should, perhaps, check this more carefully though

# permissions for any authenticated user
$DRUSH php-eval "user_role_grant_permissions(2, array(
  'use text format full_html',
	'access ckeditor link',
	'access content',
  'view the administration theme'
 )
)"

# permissions for site editor:
$DRUSH php-eval "user_role_grant_permissions(4, array(
  'access content overview', 
  'access dashboard', 
	'access overlay', 
	'access toolbar', 
	'administer blocks', 
	'administer menu', 
  'administer nodes', 
	'administer url aliases', 
	'create url aliases', 
	'view the administration theme',
	'bypass node access',
  'create new directories',
  'create new files',
  'delete files and directories',
  'paste from clipboard',
  'rename files and directories',
  'access chemistry_config pages',
  'crop any image', 
  'file uploads',
  'duplicate files',
  'resize images',
  'use file manager',
  'grant content access',
  'access statistics',
  'view post access counter',
  )
)"

# permissions for site members
$DRUSH php-eval "user_role_grant_permissions(5, array(
	 'crop any image', 
   'file uploads',
   'duplicate files',
   'resize images',
   'use file manager',
	)
)"

# probably a good idea to clear caches after messing with theme stuff
$DRUSH cc all

# rebuild content access permissions
$DRUSH php-eval "node_access_rebuild()"

popd

a2ensite $SERVERSHORTNAME
apache2ctl graceful
